import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { BaseBookingComponent } from './booking/base/base-booking.component';
import { LoginActivate } from './login/activate/login-activtate';
import { BaseLoginComponent } from './login/base/base-login.component';

const routes: Routes = [
  { path: 'login', component: BaseLoginComponent },
  { path: 'booking', component: BaseBookingComponent, canActivate:[LoginActivate] },
  { path: '**', component: BaseLoginComponent },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
