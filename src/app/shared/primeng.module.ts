import { NgModule } from '@angular/core';

import { ButtonModule } from 'primeng/button';
import { InputTextModule} from 'primeng/inputtext';
import { TableModule } from 'primeng/table';
import { ToolbarModule } from 'primeng/toolbar';
import { CalendarModule } from 'primeng/calendar';
import { DialogModule } from "primeng/dialog";

@NgModule({
  imports: [
    ButtonModule,
    InputTextModule,
    TableModule,
    ToolbarModule,
    CalendarModule,
    DialogModule,
  ],
  exports: [
    ButtonModule,
    InputTextModule,
    TableModule,
    ToolbarModule,
    CalendarModule,
    DialogModule,
  ]
})
export class PrimengModule { }
