import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable, Subject } from 'rxjs';
import { environment } from 'src/environments/environment';
import { User } from '../../shared/model/user';

@Injectable({
  providedIn: 'root'
})
export class UserService {
  private activeUser: User = User.factory()
  public activeUser$: Subject<User> = new Subject<User>()

  constructor(
  ) { }

  setActiveUser(user: User) {
    this.activeUser = user
    this.notifyNewUser()
  }

  getActiveUser(): User {
    if (this.activeUser.id === -1) {
      throw new Error("Login please");
    }
    return this.activeUser
  }

  notifyNewUser() {
    this.activeUser$.next(this.activeUser)
  }
}
