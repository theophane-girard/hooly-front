import { Booking } from "../../booking/model/booking";

export class User {
  constructor(
    public id: number,
    public username: string,
    public password: string,
    public bookings: Booking[]
  ) {}

  static factory(
    id: number = -1,
    username: string ='',
    password: string = '',
    bookings: Booking[] = []) {
    return new User(id, username, password, bookings)
  }
}
