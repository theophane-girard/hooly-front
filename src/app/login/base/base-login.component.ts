import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { catchError, Observable, Subscription } from 'rxjs';
import { UserService } from 'src/app/shared/service/user.service';
import { User } from '../../shared/model/user';
import { LoginService } from "../service/login.service";

@Component({
  selector: 'base-login',
  templateUrl: './base-login.component.html',
  styleUrls: ['./base-login.component.scss']
})
export class BaseLoginComponent implements OnInit {

  form: FormGroup = new FormGroup({
    username: new FormControl('', Validators.required),
    password: new FormControl('', Validators.required),
  })
  errMessage:string = ''

  constructor(
    private loginService: LoginService,
    private router: Router,
    private userService: UserService) {
  }

  ngOnInit(): void {
  }

  onSubmit() {
    let login$ = this.loginService.login(this.form.getRawValue())
    let loginSub: Subscription = login$
    .subscribe({
      next: (user: User) => {
        this.userService.setActiveUser(user)
        this.router.navigate(['booking'])
      },
      error: err => console.error(err),
      complete: () => loginSub.unsubscribe()
    })
  }

}
