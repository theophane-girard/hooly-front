import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { BaseLoginComponent } from './base/base-login.component';
import { LoginService } from './service/login.service';
import { SharedModule } from '../shared/shared.module';
import { LoginActivate } from './activate/login-activtate';



@NgModule({
  declarations: [
    BaseLoginComponent
  ],
  imports: [
    CommonModule,
    SharedModule
  ],
  exports: [
    BaseLoginComponent
  ],
  providers: [
    LoginService,
    LoginActivate
  ]
})
export class LoginModule { }
