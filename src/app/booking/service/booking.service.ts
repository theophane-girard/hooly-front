import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from 'src/environments/environment';
import { Booking } from "../model/booking";
import { User } from '../../shared/model/user';
import { formatDate } from '@angular/common';
import { CreateBookingRequest } from '../model/create-booking-request';

@Injectable({
  providedIn: 'root'
})
export class BookingService {

  constructor(private http: HttpClient) { }

  getBookings(id: number): Observable<Booking[]> {
    let headers = new HttpHeaders();
    let params = new HttpParams();
    headers = headers.append('Content-Type', 'application/json');
    params = params.append('id', id)

    return this.http.get<Booking[]>(environment.apiUrl + 'bookings', { headers: headers, params: params})
  }

  book(id: number, booking: Booking): Observable<any> {
    let date = formatDate(booking.date, 'dd-MM-yyyy', 'fr-FR')
    let newBookingRequest: CreateBookingRequest = CreateBookingRequest.factory(id, date)
    const httpOptions = {
      headers: new HttpHeaders({
        'Content-Type':  'application/json',
      })
    };

    return this.http.post<Booking>(environment.apiUrl + 'bookings', newBookingRequest, httpOptions)
  }
}
