import { ComponentFixture, TestBed } from '@angular/core/testing';

import { BaseBookingComponent } from './base-booking.component';

describe('BaseBookingComponent', () => {
  let component: BaseBookingComponent;
  let fixture: ComponentFixture<BaseBookingComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ BaseBookingComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(BaseBookingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
