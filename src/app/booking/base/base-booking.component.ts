import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators } from '@angular/forms';
import { ActivatedRoute, ParamMap } from '@angular/router';
import { ConfirmationService } from 'primeng/api';
import { catchError, Observable, of, Subscription, switchMap } from 'rxjs';
import { User } from 'src/app/shared/model/user';
import { UserService } from 'src/app/shared/service/user.service';
import { Booking } from '../model/booking';
import { BookingService } from '../service/booking.service';

@Component({
  selector: 'base-booking',
  templateUrl: './base-booking.component.html',
  styleUrls: ['./base-booking.component.scss'],
  providers: [ConfirmationService]
})
export class BaseBookingComponent implements OnInit {

  bookings: Booking[] = []
  isBookingDialogVisible: boolean = false
  form: FormGroup = new FormGroup({
    username: new FormControl('', Validators.required),
    date: new FormControl(new Date(), Validators.required),
  })

  constructor(
    private bookingService: BookingService,
    private route: ActivatedRoute,
    private confirmationService: ConfirmationService,
    private userService: UserService
  ) {
    route.params.subscribe(val => {
      this.bookingService.getBookings(this.userService.getActiveUser().id).pipe(
        catchError((err: Error) => of([]))
      ).subscribe({
        next: (response: Booking[]) => this.bookings = response,
        error: err => console.error(err)
      })
    })
  }

  ngOnInit(): void {
  }

  onOpenNew() {
    try {
      let user: User = this.userService.getActiveUser()
      this.form.controls['username'].setValue(user.username)
      this.isBookingDialogVisible = true;
    } catch (error) {
      console.error(error);
    }
  }

  onSubmit() {

  }

  onHideDialog() {
    this.isBookingDialogVisible = false;
  }

  onSaveBooking() {
    try {
      let booking: Booking = <Booking> this.form.getRawValue()
      let bookingSub: Subscription = this.bookingService.book(this.userService.getActiveUser().id, booking).pipe(
        switchMap((booking: Booking) => this.bookingService.getBookings(booking.user.id)),
        catchError(err => of([]))
      )
      .subscribe({
        next: bookings => {
          if (bookings.length === 0) {
            return
          }
          this.bookings = bookings
        },
        error: error => console.error(error),
        complete:() => {
          bookingSub.unsubscribe()
          this.isBookingDialogVisible = false;
        }
      })
    } catch (error) {
      console.error(error);
    }
  }

}
