import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { BaseBookingComponent } from './base/base-booking.component';
import { HttpClientModule } from '@angular/common/http';
import { PrimengModule } from '../shared/primeng.module';
import { BookingService } from './service/booking.service';
import { SharedModule } from '../shared/shared.module';
import { UserService } from '../shared/service/user.service';


@NgModule({
  declarations: [
    BaseBookingComponent
  ],
  imports: [
    CommonModule,
    SharedModule,
  ],
  exports: [
    BaseBookingComponent
  ],
  providers:[
    BookingService
  ]
})
export class BookingModule { }
