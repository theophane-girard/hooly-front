export class CreateBookingRequest {
  constructor(
    public userId: number,
    public date: string
  ){}

  static factory(userId: number,date: string) {
    return new CreateBookingRequest(userId, date)
  }
}
