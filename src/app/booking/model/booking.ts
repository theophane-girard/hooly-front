import { User } from "src/app/shared/model/user";

export class Booking {
  constructor(
    public id: number,
    public date: Date,
    public user: User
  ){}

  static factory(id: number, date: Date = new Date(), user: User = User.factory()) {
    return new Booking(id, date, user)
  }
}
